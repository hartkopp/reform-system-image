#!/bin/bash

set -x
set -e

export ARCH=arm64
export CROSS_COMPILE=aarch64-linux-gnu-
export KBUILD_BUILD_TIMESTAMP=$(date --date="@$SOURCE_DATE_EPOCH" --rfc-email)
export KBUILD_BUILD_VERSION=1
export KBUILD_BUILD_USER=reformuser
export KBUILD_BUILD_HOST=reformhost

if [ ! -d linux ]
then
  echo "Cloning Linux..."
  git clone --depth 1 --branch v5.12 https://github.com/torvalds/linux.git
fi

cp ./template-kernel/*.dts ./linux/arch/arm64/boot/dts/freescale/
cp ./template-kernel/kernel-config ./linux/.config

cd linux

echo "== kernel config =="
cat .config
echo "== end kernel config =="

for PATCHFILE in ../template-kernel/patches/*.patch
do
  echo PATCH: $PATCHFILE
  if git apply --check $PATCHFILE; then
    git apply $PATCHFILE
  else
    echo "\e[1mKernel patch already applied or cannot apply: $PATCHFILE"
  fi
done

make -j$(nproc) Image freescale/imx8mq-mnt-reform2.dtb freescale/imx8mq-mnt-reform2-hdmi.dtb

cd ..
