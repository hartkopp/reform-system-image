#!/bin/bash

DISK=""
if [ -e /dev/loop0p1 ]
then
	DISK="-drive if=none,file=/dev/loop0p1,id=rfs -device virtio-blk-device,drive=rfs"
else
	echo "Hint: mount the disk image with: udisksctl loop-setup -f ./reform-system.img"
fi

qemu-system-aarch64 \
  -machine virt \
  -cpu cortex-a53 \
  -smp 4 \
  -kernel ./linux/arch/arm64/boot/Image \
  -device virtio-gpu-pci,virgl=on \
  -append "console=ttyAMA0 root=/dev/vda rw" \
  -m 4096 \
  -nic user,model=virtio-net-pci \
  -display gtk,gl=on \
  -device usb-ehci -device usb-kbd -device usb-tablet -usb \
  -serial stdio \
  $DISK


